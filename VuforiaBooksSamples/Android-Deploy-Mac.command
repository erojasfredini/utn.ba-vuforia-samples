DIRNAME=`dirname "$0"`
echo Using Unity 2019.2.3f1 located under standard folder. The apk should be inside Bin/Android/VuforiaBooksSamples.apk
echo -----------------------
echo Listing Android Devices
echo -----------------------
/Applications/Unity/Hub/Editor/2019.2.3f1/PlaybackEngines/AndroidPlayer/SDK/platform-tools/adb devices
echo -----------------------
echo Installing APK
echo -----------------------
/Applications/Unity/Hub/Editor/2019.2.3f1/PlaybackEngines/AndroidPlayer/SDK/platform-tools/adb install $DIRNAME/Bin/Android/VuforiaBooksSamples.apk
echo Done!