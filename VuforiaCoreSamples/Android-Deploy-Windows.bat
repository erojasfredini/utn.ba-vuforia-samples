@echo off
echo Using Unity 2019.2.3f1 located under standard folder in C drive. The apk should be inside Bin/Android/VuforiaCoreSamples.apk
echo -----------------------
echo Listing Android Devices
echo -----------------------
"C:\Program Files\Unity\Hub\Editor\2019.2.3f1\Editor\Data\PlaybackEngines\AndroidPlayer\SDK\platform-tools\adb.exe" devices
echo -----------------------
echo Installing APK
echo -----------------------
"C:\Program Files\Unity\Hub\Editor\2019.2.3f1\Editor\Data\PlaybackEngines\AndroidPlayer\SDK\platform-tools\adb.exe" install Bin/Android/VuforiaCoreSamples.apk
echo Done!
pause